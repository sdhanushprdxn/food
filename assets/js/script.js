window.onload = () => {
  const submit = document.getElementById('submit');
  const name = document.getElementById('name');
  const materialName = document.getElementById('material');
  const inputArea = document.getElementsByClassName('inputarea')[0];
  let counter = 0 ;
  let count = 0;
  let store = [];

  //to del elements in iput of items
  const newstore = (s) => {
    store.splice( s,1);
  }

  const delEl = () => {
    const removeElement = document.getElementsByClassName('remove');
    for (let j = 0; j < removeElement.length; j++){
      removeElement[j].addEventListener('click',function(){
        this.parentElement.remove();
        counter = removeElement.length;
        newstore(j);
      })
    }
  }

  // creat label on pressing space
  const labelCreate = (item) => {
    const span = document.createElement('span');
    span.className = 'item'
    span.innerText = item;
    const remove = document.createElement('span');
    remove.className = 'remove'
    remove.innerText='X';
    span.appendChild(remove)
    inputArea.insertBefore(span, inputArea.childNodes[counter]);
    counter++; 
    delEl();
  }
  
  //event on input of food items
  const myfun= () => {
    let arr=[];
    let msg=materialName.value; 
    let tmp="";
    for (let i = 0; i < msg.length; i++) {
      if (msg.charAt(i) !==" ") {
        tmp += msg.charAt(i);
      }
       else {
        arr[arr.length] = tmp;
        store.push(...arr);
        console.log(store)
        console.log(arr)
        labelCreate(arr[0])
        tmp='';
        materialName.value = "";
      }
    }
  }
  materialName.addEventListener('input',myfun);

  // on submit create table
  const onSub = (e) => {
    count++
    e.preventDefault();
    const srli =document.getElementsByClassName('sr')[0];
    const foNa =document.getElementsByClassName('foodNa')[0];
    const item =document.getElementsByClassName('items')[0];
    const delbtn =document.getElementsByClassName('delbtn')[0];
    const upbtn =document.getElementsByClassName('updbtn')[0];
    const numb = document.createElement('li')
    numb.className = 'indexVal';
    numb.innerText = count;
    srli.appendChild(numb)
    const foodNa = document.createElement('li')
    foodNa.innerText = name.value;
    foNa.appendChild(foodNa)
    const foodItem = document.createElement('li')
    foodItem.innerText = store;
    item.appendChild(foodItem);
    const delRow = document.createElement('li')
    const delanc = document.createElement('a')
    delanc.className = 'btnDel';
    delanc.innerText = 'Delete';
    delanc.title = 'Delete';
    delRow.appendChild(delanc);
    delbtn.appendChild(delRow);
    const update = document.createElement('li')
    const upanc = document.createElement('a')
    upanc.innerText = 'Update'
    upanc.title = 'Update';
    update.appendChild(upanc);
    upbtn.appendChild(update);  
    name.value = ''; 
    store = [];
    counter=0
    const removeElements = (elms) => elms.forEach(el => el.remove());
    removeElements( document.querySelectorAll(".item") );

    //to del whole row
    var getfun = (e) =>{
      let li = e.target.closest('li');
      let nodes = [... li.closest('ul').children];
      var index = nodes.indexOf( li );
      srli.children[index].remove();
      foNa.children[index].remove();
      item.children[index].remove();
      delbtn.children[index].remove();
      upbtn.children[index].remove();
      const indexing = document.getElementsByClassName('indexVal');
      for (let b=0 ; b < indexing.length; b++) {
        indexing[b].innerHTML= b+1;
      }
    }  

    const btndel = document.getElementsByClassName('btnDel');
    for (a=0; a < btndel.length; a++) {
      btndel[a].addEventListener('click',getfun);
    }
  }
  submit.addEventListener('click',onSub);
}